
package enums;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour Ingredient.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * <p>
 * <pre>
 * &lt;simpleType name="Ingredient">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Poulet"/>
 *     &lt;enumeration value="Viande"/>
 *     &lt;enumeration value="Poivron"/>
 *     &lt;enumeration value="Tomate"/>
 *     &lt;enumeration value="Mozzarella"/>
 *     &lt;enumeration value="Aubergine"/>
 *     &lt;enumeration value="Jambon"/>
 *     &lt;enumeration value="Champignon"/>
 *     &lt;enumeration value="Ananas"/>
 *     &lt;enumeration value="Chevre"/>
 *     &lt;enumeration value="Miel"/>
 *     &lt;enumeration value="Saumon"/>
 *     &lt;enumeration value="Emmental"/>
 *     &lt;enumeration value="Chorizo"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "Ingredient", namespace = "https://www.univ-grenoble-alpes.fr/francais/l3miage/Ingredient")
@XmlEnum
public enum Ingredient {

    @XmlEnumValue("Poulet")
    Poulet("Poulet"),
    @XmlEnumValue("Viande")
    Viande("Viande"),
    @XmlEnumValue("Poivron")
    Poivrons("Poivron"),
    @XmlEnumValue("Tomate")
    Tomates("Tomate"),
    @XmlEnumValue("Mozzarella")
    Mozzarella("Mozzarella"),
    @XmlEnumValue("Legume")
    Légumes("Legume"),
    @XmlEnumValue("Jambon")
    Jambon("Jambon"),
    @XmlEnumValue("Champignon")
    Champignons("Champignon"),
    @XmlEnumValue("Ananas")
    Ananas("Ananas"),
    @XmlEnumValue("Chevre")
    Chèvre("Chevre"),
    @XmlEnumValue("Miel")
    Miel("Miel"),
    @XmlEnumValue("Saumon")
    Saumon("Saumon"),
    @XmlEnumValue("Emmental")
    Emmental("Emmental"),
    @XmlEnumValue("Chorizo")
    Chorizo("Chorizo"),

    @XmlEnumValue("Creme_fraiche")
    Crème_fraiche("Creme_fraiche"),
    @XmlEnumValue("Sauce_barbecue")
    Sauce_barbecue("Sauce_barbecue"),
    @XmlEnumValue("Boeuf")
    Boeuf("Boeuf"),
    @XmlEnumValue("Merguez")
    Merguez("Merguez"),
    @XmlEnumValue("Pomme_de_terre")
    Pomme_de_terre("Pomme_de_terre"),
    @XmlEnumValue("Lardon")
    Lardons("Lardon"),
    @XmlEnumValue("Fromage")
    Fromage("Fromage"),
    @XmlEnumValue("Sauce_tomate")
    Sauce_tomate("Sauce_tomate"),
    @XmlEnumValue("Moutarde")
    Moutarde("Moutarde"),
    @XmlEnumValue("Ail_et_fine_herbes")
    Ail_et_fines_herbes("Ail_et_fine_herbes"),
    @XmlEnumValue("Saucisse")
    Saucisse("Saucisse"),
    @XmlEnumValue("Choufleur")
    Chou_fleur("Choufleur"),
    @XmlEnumValue("Oignons")
    Oignons("Oignons"),
    @XmlEnumValue("Flagolets")
    Flageolets("Flagolets"),
    @XmlEnumValue("Poisson")
    Poisson("Poisson"),
    @XmlEnumValue("Nutella")
    Nutella("Nutella"),
    @XmlEnumValue("Banane")
    Banane("Banane"),
    @XmlEnumValue("mm_s")
    mlllmppps("mm_s"),
    @XmlEnumValue("Chantilly")
    Chantilly("Chantilly");

    private final String value;

    Ingredient(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static Ingredient fromValue(String v) {
        for (Ingredient c: Ingredient.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}

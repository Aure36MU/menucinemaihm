import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StepperPersoComponent } from './stepper-perso.component';

describe('StepperPersoComponent', () => {
  let component: StepperPersoComponent;
  let fixture: ComponentFixture<StepperPersoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StepperPersoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StepperPersoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

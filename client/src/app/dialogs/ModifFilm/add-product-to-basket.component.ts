import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {BasketService} from '../../basket/basket.service';
import {Product} from '../../product/class/Product';
import {FoodService} from '../../product/food/food.service';
import {MenuClass} from '../../product/menu/menu';
import {MenuService} from '../../product/menu/menu.service';
import {ProductGroupInterface} from '../../interface/ProductInterface';
import {ApiService} from '../../services/api.service';
import {ProductType} from '../../enum/ProductType';
import {DBProductType} from '../../enum/DBProductType';
import {RecommandationService} from '../../recommandation/recommandation.service';
import {timeout} from "rxjs/operators";


// a refactoriser
@Component({
    selector: 'app-add-food-to-cart',
    templateUrl: './add-product-to-basket.component.html',
    styleUrls: ['./add-product-to-basket.component.scss']
})

// modif fabien
export class AddProductToBasketComponent implements OnInit {
    private amount = 1;
    private price = 0;
    private _extraFromage = 0;
    private _extraBacon = 0;
    private _pateEpaisse = 0;

    constructor(public dialogRef: MatDialogRef<AddProductToBasketComponent>,
                @Inject(MAT_DIALOG_DATA) public data: Product,
                private basketService: BasketService,
                private foodService: FoodService,
                private menuService: MenuService,
                private apiService: ApiService,
                private recommandationService: RecommandationService) {

    }
    // temporaire fabien
    ngOnInit() {
        this.data.prix = this.data.nouveauPrix;
        this.calculatePrice();
    }

    private calculatePrice() {
        this.price = (this.data.prix + this._extraBacon + this._extraFromage + this._pateEpaisse) * this.amount;
    }

    isMenu() {
        return this.data instanceof MenuClass;
    }

    close() {
        this.dialogRef.close();
    }
    // temporaire fab
    async onValid() {
        let productGroup: ProductGroupInterface<any>;
        this.data.prix = this.price;
        this.data.extraFromage = this._extraFromage;
        this.data.extraBacon = this._extraBacon;
        productGroup = {product: this.data, amount: this.amount};
        this.dialogRef.close(productGroup);
    }


    getSearchType() {
        return this.recommandationService.getSearchType(this.data.type);
    }

    getGivenType() {
        return this.recommandationService.getGivenType(this.data.type);

    }
    extraFromage() {
        if (this._extraFromage !== 0)  {
            this._extraFromage = 0;
        } else {
            this._extraFromage = 1;
        }
        this.calculatePrice();
    }
    extraBacon() {
        if (this._extraBacon !== 0)  {
            this._extraBacon = 0;
        } else {
            this._extraBacon = 1.5;
        }
        this.calculatePrice();
    }
    typePate(n: number) {
        if (n === 1) {
            this._pateEpaisse = 0;
            this.data.typePate = 'pate standart';
        }
        if (n === 2) {
            this._pateEpaisse = 1;
            this.data.typePate = 'pate épaisse';
        }
        if (n === 3) {
            this._pateEpaisse = 2.5;
            this.data.typePate = 'pate au fromage';
        }
        this.calculatePrice();
    }
}

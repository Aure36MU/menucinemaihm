import {ProductType} from '../enum/ProductType';

export interface FoodInterface {
    id: number;
    type: ProductType;
    nom: string;
    description: string;
    prix: number;
    photo: string;
    categorie: number;
    ingredients: string [];
    extraFromage: number;
    extraBacon: number;
    typePate: string;
}

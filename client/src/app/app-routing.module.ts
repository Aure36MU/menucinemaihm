import {NgModule} from '@angular/core';
import {Routes, RouterModule, ROUTES, PreloadAllModules} from '@angular/router';
import {AuthGuard} from "./auth/auth.guard";
import {MovieComponent} from "./product/movie/movie.component";
import {LoadingComponent} from "./loading/loading.component";

const routes: Routes = [
    {path: '', canActivate: [AuthGuard], component: LoadingComponent}
];

@NgModule({
   imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })],
    exports: [RouterModule]
})
export class AppRoutingModule {
}

import { Component, OnInit } from '@angular/core';
import {MatDialog} from "@angular/material/dialog";
import {BasketService} from "../basket/basket.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.scss']
})
export class LoadingComponent implements OnInit {

  constructor(public dialog: MatDialog,
              private basketService: BasketService,
              private router: Router) { }

  ngOnInit() {
    if (this.basketService.hasMovie()) {
      console.log("loading redirects to product");
      this.router.navigate(['/product']);
    } else {
      console.log("loading redirects to homepage");
      this.router.navigate(['/homepage']);
    }

  }

}

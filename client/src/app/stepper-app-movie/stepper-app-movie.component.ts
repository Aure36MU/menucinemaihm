import {Component, Input, OnInit} from '@angular/core';
import {NavBarService} from "../services/nav-bar.service";
import {calcPossibleSecurityContexts} from "@angular/compiler/src/template_parser/binding_parser";

@Component({
  selector: 'app-stepper-app-movie',
  templateUrl: './stepper-app-movie.component.html',
  styleUrls: ['./stepper-app-movie.component.scss']
})
export class StepperAppMovieComponent implements OnInit {

  @Input() whatPage: number;
  indexSelect: number;
  constructor(private navBarService: NavBarService) { }

  ngOnInit() {
    this.navBarService._monLienSubject.subscribe(
        data => {
          if(data === 'moviePage') {
            this.indexSelect = 0;
          }
          if(data === 'pizzaPage') {
            this.indexSelect = 1;
          }
          if(data === 'paymentPage') {
            this.indexSelect = 2;
          }
        },
        err => {
          console.log('error');
        },
        () => {
        });
  }

  info() {
    
  }

  rootage(nbr: number) {
    
  }
}

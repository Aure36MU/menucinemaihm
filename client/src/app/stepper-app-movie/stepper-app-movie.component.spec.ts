import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StepperAppMovieComponent } from './stepper-app-movie.component';

describe('StepperAppMovieComponent', () => {
  let component: StepperAppMovieComponent;
  let fixture: ComponentFixture<StepperAppMovieComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StepperAppMovieComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StepperAppMovieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {ProductInterface} from '../../interface/ProductInterface';
import {ProductType} from "../../enum/ProductType";

export abstract class Product implements ProductInterface {
    public id: number;
    public prix: number;
    public type: ProductType;
    public nouveauPrix: number;
    public extraBacon: number;
    public extraFromage: number;
    public typePate: string;


    constructor(id: number, prix: number, type: ProductType, nouveauPrix: number, extraBacon: number, extraFromage: number, typePate: string) {
        this.id = id;
        this.prix = prix;
        this.type = type;
        this.nouveauPrix = nouveauPrix;
        this.extraBacon = extraBacon;
        this.extraFromage = extraFromage;
        this.typePate = typePate;
    }
}

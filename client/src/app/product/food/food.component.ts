import {Component, Input, EventEmitter, OnInit, Output} from '@angular/core';
import {ProductService} from '../product.service';
import {MatDialog, MatSnackBar} from '@angular/material';
import {AddProductToBasketComponent} from '../../dialogs/add-product-to-basket/add-product-to-basket.component';
import {Food} from './food';
import {ProductGroupInterface} from '../../interface/ProductInterface';
import {BasketService} from '../../basket/basket.service';
import {FoodGroup} from './foodGroup';
import {Product} from '../class/Product';
import {AddEventInterface} from '../../interface/AddEventInterface';
import {AddEventType} from '../../enum/AddEventType';
import {ProductType} from '../../enum/ProductType';
import {environment} from '../../../environments/environment';
import {StepperService} from '../../stepper-perso/stepper.service';

@Component({
    selector: 'app-food',
    templateUrl: './food.component.html',
    styleUrls: ['./food.component.scss']
})
export class FoodComponent implements OnInit {
    @Input() private food: Food;
    @Input() private product: Product;

    @Output() private addEvent: EventEmitter<AddEventInterface> = new EventEmitter();
    @Input() private type: ProductType;
    @Input() private products: Product[];
    @Input() private eventType: AddEventType;

    constructor(private foodService: ProductService,
                public dialog: MatDialog,
                public basket: BasketService,
                public snackBar: MatSnackBar,
                public stepper: StepperService) {
    }

    ngOnInit() {
        this.checkFood(this.food);
    }

    private checkFood(food: Food) {
        if (food == null) {
            throw new Error('Food needed');
        }
    }

    // modif fabien
    openDialog() {
        const dialogRef = this.dialog.open(AddProductToBasketComponent, {
            width: '50%',
            minWidth: '600px',
            maxHeight: '1000px',
            // height: '75%',
            data: this.product,
            closeOnNavigation: true
        });
        dialogRef.afterClosed().toPromise().then((productGroupInterface: ProductGroupInterface<any>) => {
            if (productGroupInterface !== undefined) {
                const addEvent: AddEventInterface = {
                    data: productGroupInterface,
                    event: this.eventType
                };
                this.basket.onAdd(addEvent);
            }
        });
    }

    // modif fab
  private onValid() {
      // tslint:disable-next-line:prefer-const
      const productGroupInterface: ProductGroupInterface<Product> = {
          product: this.product,
          amount: 1
      };
      const addEvent: AddEventInterface = {
          data: productGroupInterface,
          event: this.eventType
      };
      this.basket.onAdd(addEvent);
      this.snackBar.open( 'La pizza ' + this.food.nom + ' a été ajoutée au panier', this.food.prix + '€', {
          duration: 8000,
      });
      this.stepper.desactivEtat();
    }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FoodComponent} from './food.component';
import {MatCardModule} from '@angular/material';

@NgModule({
  declarations: [
      FoodComponent
  ],
    imports: [
        CommonModule,
        MatCardModule
    ], exports: [
      FoodComponent
    ]
})
export class FoodModule { }

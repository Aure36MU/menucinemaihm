import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Router} from '@angular/router';
import {environment} from '../../../../environments/environment';
import {NavBarService} from '../../../services/nav-bar.service';
import {TmdbService} from '../../../services/tmdb.service';
import {AuthService} from '../../../auth/auth.service';

@Component({
  selector: 'app-nav-bar-pizza',
  templateUrl: './nav-bar-pizza.component.html',
  styleUrls: ['./nav-bar-pizza.component.scss']
})
export class NavBarPizzaComponent implements OnInit {

  @Output() searchEvent: EventEmitter<string> = new EventEmitter();

// ici on a modifié le  constructor pour faire appel au navbar.service
  // et on a ajouté des fonctions en bas de la page pour transmettre les variables
  constructor(public authService: AuthService,
              private router: Router,
              private navbar: NavBarService) {
  }

  ngOnInit(): void {
  }

  private signOut() {
    this.authService.signOut();
  }

  goToProfile() {
    this.router.navigate(['/profile']);
  }


  getLogoPath() {
    return `${environment.apiBaseUrl}photo/icon.png`;
  }

  goToHomepage() {
    this.router.navigate(['/']);
  }

  /**
   * A chaque changement de recherche dans la bar de recherche, obtient la nouvelle string recherché
   * @param query : la nouvelle string recherché
   */

  get searchString(): string {
    return this.navbar.searchString;
  }

  set searchString(value: string) {
    this.navbar.searchString = value;
  }


}

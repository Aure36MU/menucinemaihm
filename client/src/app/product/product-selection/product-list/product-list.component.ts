import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ProductType} from '../../../enum/ProductType';
import {MatDialog} from '@angular/material';
import {AddEventInterface} from '../../../interface/AddEventInterface';
import {Product} from '../../class/Product';
import {Food} from '../../food/food';
import {MenuClass} from '../../menu/menu';
import {AddProductToBasketComponent} from '../../../dialogs/add-product-to-basket/add-product-to-basket.component';
import {ProductGroup} from '../../class/productGroup';
import {AddEventType} from '../../../enum/AddEventType';
import {environment} from '../../../../environments/environment';
import {ProductGroupInterface} from '../../../interface/ProductInterface';
import {ActivatedRoute, Router} from '@angular/router';
import {SearchMenuInterface} from '../../../interface/SearchInterface';


@Component({
    selector: 'app-product-list',
    templateUrl: './product-list.component.html',
    styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {
    @Output() private addEvent: EventEmitter<AddEventInterface> = new EventEmitter();
    @Input() private type: ProductType;
    @Input() private products: Product[];
    @Input() private eventType: AddEventType;
    private gridContent: Product[][];
    private foods: Food[][];

    constructor(public dialog: MatDialog,
                private router: Router) {

    }

    ngOnInit() {
        this.gridContent = [];
        this.foods = []
        this.initGridContent();
    }

    isMenu(product: Product) {
        return product instanceof MenuClass;
    }

    toFood(product) {
        return product as Food;
    }


    initGridContent() {
        for (let i = 0, j = 0; i < this.products.length; i++) {
            if (i >= environment.nbItemsPerRow && i % environment.nbItemsPerRow === 0) {
                j++;
            }
            this.gridContent[j] = this.gridContent[j] || [];
            this.gridContent[j].push(this.products[i]);
        };
        this.products.forEach((product: Product) => {
            // this.foods.push(product);
            const food: Food = this.toFood(product);
            // @ts-ignore
            this.foods.push(food);
        });
    }


    toMenu(product: Product) {
        return product as MenuClass;
    }

    info() {
        console.log('info');
    }

    rootage(root : number) {
        if (root === 1 ) {
            this.router.navigate(['/']);
        }
    }


}

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FoodComponent} from '../food/food.component';
import {ProductListComponent} from './product-list/product-list.component';
import {ProductSelectionComponent} from './product-selection.component';
import {MatIcon, MatTabsModule} from '@angular/material';
import {DialogsModule} from '../../dialogs/dialogs.module';
import { NavBarPizzaComponent } from './nav-bar-pizza/nav-bar-pizza.component';
import {MatIconModule} from "@angular/material/icon";
import {BasketModule} from "../../basket/basket.module";

@NgModule({
    declarations: [NavBarPizzaComponent],
    imports: [
        CommonModule,
        MatTabsModule,
        MatIconModule,
        BasketModule
    ], exports: [
        NavBarPizzaComponent
    ]
})
export class ProductSelectionModule {
}

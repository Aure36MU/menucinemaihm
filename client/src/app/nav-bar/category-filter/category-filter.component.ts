import { Component, OnInit } from '@angular/core';
import {TmdbService} from '../../services/tmdb.service';
import {AuthService} from '../../auth/auth.service';
import {Router} from '@angular/router';
import {NavBarService} from '../../services/nav-bar.service';

@Component({
  selector: 'app-category-filter',
  templateUrl: './category-filter.component.html',
  styleUrls: ['./category-filter.component.scss']
})
export class CategoryFilterComponent implements OnInit {

  constructor(private tmdb: TmdbService,
              public authService: AuthService,
              private router: Router,
              private navbar: NavBarService) {
  }

  ngOnInit() {
  }

  action() {
  //  if (this.navbar.action) { this.navbar.action = false; } else { this.navbar.action = true; }
  }
  aventure() {
  //  if (this.navbar.aventure) { this.navbar.aventure = false; } else { this.navbar.aventure = true; }
  }
  comedie() {
   // if (this.navbar.comedie) { this.navbar.comedie = false; } else { this.navbar.comedie = true; }
  }
  horreur() {
  //  if (this.navbar.horreur) { this.navbar.horreur = false; } else { this.navbar.horreur = true; }
  }


}

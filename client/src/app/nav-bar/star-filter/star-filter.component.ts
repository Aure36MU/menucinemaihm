import { Component, OnInit } from '@angular/core';
import {TmdbService} from '../../services/tmdb.service';
import {AuthService} from '../../auth/auth.service';
import {Router} from '@angular/router';
import {NavBarService} from '../../services/nav-bar.service';

@Component({
  selector: 'app-star-filter',
  templateUrl: './star-filter.component.html',
  styleUrls: ['./star-filter.component.scss']
})
export class StarFilterComponent implements OnInit {

  constructor(private tmdb: TmdbService,
              public authService: AuthService,
              private router: Router,
              private navbar: NavBarService) {
  }

  ngOnInit() {
  }

  rating1() {
    this.navbar.searchRating = 2;
  }
  rating2() {
    this.navbar.searchRating = 4;
  }
  rating3() {
    this.navbar.searchRating = 6;
  }
  rating4() {
    this.navbar.searchRating = 8;
  }
  rating5() {
    this.navbar.searchRating = 9;
  }

}

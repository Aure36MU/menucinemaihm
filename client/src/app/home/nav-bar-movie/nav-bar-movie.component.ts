import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {TmdbService} from '../../services/tmdb.service';
import {AuthService} from '../../auth/auth.service';
import {Router} from '@angular/router';
import {environment} from '../../../environments/environment';
import {NavBarService} from '../../services/nav-bar.service';
import {BasketService} from '../../basket/basket.service';


@Component({
  selector: 'app-nav-bar-movie',
  templateUrl: './nav-bar-movie.component.html',
  styleUrls: ['./nav-bar-movie.component.scss']
})
export class NavBarMovieComponent implements OnInit {

  @Output() searchEvent: EventEmitter<string> = new EventEmitter();
  private activeMenu: boolean;

// ici on a modifié le  constructor pour faire appel au navbar.service
  // et on a ajouté des fonctions en bas de la page pour transmettre les variables
  constructor(private tmdb: TmdbService,
              public authService: AuthService,
              private router: Router,
              private navbar: NavBarService,
              private basket: BasketService) {
    this.activeMenu = true;
  }

  ngOnInit(): void {
  }

  private signOut() {
    this.authService.signOut();
  }

  goToProfile() {
    this.router.navigate(['/profile']);
  }


  getLogoPath() {
    return `${environment.apiBaseUrl}photo/icon.png`;
  }

  goToHomepage() {
    this.router.navigate(['/']);
  }

  /**
   * A chaque changement de recherche dans la bar de recherche, obtient la nouvelle string recherché
   * @param query : la nouvelle string recherché
   */
  public onSearch(query: string) {
    this.searchString = query;
  }


  get searchString(): string {
    return this.navbar.searchString;
  }

  set searchString(value: string) {
    this.navbar.searchString = value;
    if ( value !== '') {
      this.activeMenu = false;
    } else {this.activeMenu = true; }
  }

  getExistFoods() {
    if (this.basket.getExistFoods()) {
      return true;
    } else { return false; }
  }

  getExistMovie() {
    if (this.basket.getExistMovie()) {
      return true;
    } else { return false; }
  }


}







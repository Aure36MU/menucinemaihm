
import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {TmdbService} from '../../../services/tmdb.service';
import {AuthService} from '../../../auth/auth.service';
import {Router} from '@angular/router';
import {NavBarService} from '../../../services/nav-bar.service';


@Component({
  selector: 'app-movie-category-filter',
  templateUrl: './movie-category-filter.component.html',
  styleUrls: ['./movie-category-filter.component.scss']
})
export class MovieCategoryFilterComponent implements OnInit {
  @Input() activeMenu: boolean;
  private queryStringCate: string;
  private tblCat: string[];
  constructor(private tmdb: TmdbService,
              public authService: AuthService,
              private router: Router,
              private navbar: NavBarService) {
    this.queryStringCate = '';
  }

  ngOnInit() {
  }

  addCategorie(cat){
    if (this.queryStringCate.includes(cat)){
      console.log("CONTAned");
      this.tblCat = this.queryStringCate.split(',');
      this.tblCat = this.tblCat.filter(c => c !== cat);
      this.queryStringCate = this.tblCat.join(',');
    } else {
      if (this.queryStringCate.length === 0) {
        this.queryStringCate = this.queryStringCate + cat;
      } else {
        this.queryStringCate = this.queryStringCate + ',' + cat;
      }
    }
    console.log(this.queryStringCate);
    this.navbar.categs = this.queryStringCate;


  }

}




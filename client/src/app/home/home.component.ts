// NEW CODE

import {Component, HostListener, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {MovieResult, SearchMovieQuery, SearchMovieResponse} from '../tmdb-data/searchMovie';
import {TmdbService} from '../services/tmdb.service';
import {NavBarService} from '../services/nav-bar.service';
import {MovieInfoComponent} from '../dialogs/movie-info/movie-info.component';
import {ProductGroupInterface} from '../interface/ProductInterface';
import {BasketService} from '../basket/basket.service';
import {Router} from '@angular/router';
import {debounceTime, distinctUntilChanged} from "rxjs/operators";
import {MatSnackBar} from '@angular/material';
import {StepperService} from '../stepper-perso/stepper.service';
import {Title} from '@angular/platform-browser';
import {MatDialog} from '@angular/material';


const maxPage = 1000;

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss'],
    providers : [Title],
})
export class HomeComponent implements OnInit, OnChanges {

    // sur cette page on a modifié les variable, le constructor, le ngonInit, le ngOnChange, le searchMovie,


    moviesFound: SearchMovieResponse[] = [];
    // création des variables pour les utiliser dans la fonction searchstring
    // tslint:disable:variable-name
    @Input() private _searchString;
    @Input() private _searchRating = 0;
    @Input() private categIn = '';


    private _selectedTab = 1;
    private sortName = false;
    private sortScore = false;


    // par defaut on affiche on fait appel a searchmovie pour afficher les topmovie
    constructor(private tmdbService: TmdbService,
                private navBarService: NavBarService,
                public dialog: MatDialog,
                private basketService: BasketService,
                private router: Router,
                public snackBar: MatSnackBar,
                public stepper: StepperService,
                private title: Title) {
        // surblindage pour éviter les cas de navigation manuelle
        this.title.setTitle('MenuCinema - Film');
        if (this.basketService.hasMovie()) {
            this.router.navigate(['/product']);
        }

        this.searchMovie(this._searchString, this._selectedTab);
        this._selectedTab++;
        this.searchMovie(this._searchString, this._selectedTab);
        this.navBarService.monLien = 'moviePage';
    }

    get allMovies(): MovieResult[] {
        return  this.moviesFound.reduce( (acc, mf) => [...acc, ...mf.results], []);
    }
    get allMoviestest(): MovieResult[] {
       var toRet = this.allMovies;
       if (this._searchRating !== 0 && this._searchRating !== null) {
           toRet = toRet.filter( m => m.vote_average >= this._searchRating);
       }
       if (this.categIn !== null && this.categIn !== '') {
           const cats = this.categIn.split(',');
          //we should implement the selection of movies by categs
           toRet = toRet.filter(m => m.genre_ids.some((val) => cats.indexOf(val.toString()) !== -1));
        }
       return toRet;
    }
    // ici on met en place des subscribe qui interceptent les modifications des variables dans le navbar.service
    ngOnInit() {
        // évement qui se produit quand une recherche est fait dans la search bar
        this.navBarService._SearchStringSubject.pipe(
            debounceTime(500),
            distinctUntilChanged()
        ).subscribe(
                data => {
                    this.moviesFound = [];
                    this._selectedTab = 1;
                    this._searchString = data;
                    this.searchMovie(this._searchString, this._selectedTab);
                    this._selectedTab++;
                    this.searchMovie(this._searchString, this._selectedTab);
                },
                err => {
                    console.log('error');
                },
                () => {
                }
            );
        // évement qui se produit quand une recherche est fait sur les notes
        this.navBarService._searchRatingSubject.subscribe(
            data => {
                this.moviesFound = [];
                this._selectedTab = 1;
                this._searchRating = data;
                this.searchMovie(this._searchString, this._selectedTab);
                this._selectedTab++;
                this.searchMovie(this._searchString, this._selectedTab);
            },
            err => {
                console.log('error');
            },
            () => {
            }
        );

        //evenement produit quand un ajout de categorie ce fait:
        this.navBarService._catArrSubject
            .subscribe(
                data => {
                    this.moviesFound = [];
                    this._selectedTab = 1;
                    this.categIn = data;
                    this.searchMovie(this._searchString, this._selectedTab);
                    this._selectedTab++;
                    this.searchMovie(this._searchString, this._selectedTab);
                },
                err => {
                    console.log('error');
                },
                () => {
                }
            );

    }


// OPEN DIALOG INFORMATION:
    openDialog(movId: number) {
        const dialogRef = this.dialog.open(MovieInfoComponent, {
            width: '90vw',
            height: '70vh',
            data: movId,
            closeOnNavigation: true
        });

        dialogRef.afterClosed().toPromise().then((productGroupInterface: ProductGroupInterface<any>) => {
        });
    }

    /**
     * A chaques changements des inputs, effectue une nouvelle recherche, se replace à la première page et remet à false les boolean de tri
     */
    ngOnChanges(changes: SimpleChanges) {
    }


    /**
     * Recherche d'un film en fonction d'une query et d'une page
     * @param query : la phrase ou mot à rechercher
     * @param page : la page selectionné
     */
    // si la query est vide, on fait un appel API a TopMovie, si la query n'est pas vide, on fait un appel API a searchMovie
    public searchMovie(query: string, page: number) {
        if (query !== '' && query !== undefined) {
            const searchMovie: SearchMovieQuery = {
                query,
                region: 'fr',
                page
            };
            this.tmdbService.searchMovie(searchMovie).then((response: SearchMovieResponse) => {
                // ici test
                this.moviesFound.push(response);
                // this.allMovies.movies[this._selectedTab - 1] = response;
            });
        } else {
            const searchMovie: SearchMovieQuery = { // a finir
                query,
                region: 'fr',
                page
            };
            // tslint:disable-next-line:max-line-length
            this.tmdbService.TopMovie(searchMovie, this._searchRating, this.categIn , this._selectedTab).then((response: SearchMovieResponse) => {
                this.moviesFound.push(response);
              //  this.allMovies.movies[this._selectedTab - 1] = response;
            });
        }
    }

    addToCart(movie) {
        this.basketService.addMovie(movie);
        this.stepper.desactivEtat();
        this.router.navigate(['/product']);
        this.snackBar.open('Le film a été ajouté au panier.', '5€', {
            duration: 8000,
        });
    }

    upTopScroll() {
        window.scroll(0, 0);
    }
    // load another Movie page on scroll down to the end of page
    @HostListener('window:scroll', ['$event'])
    onWindowScroll() {
        const pos = (document.documentElement.scrollTop || document.body.scrollTop) + document.documentElement.offsetHeight;
        const max = document.documentElement.scrollHeight;
        if (pos === max )   {
            this._selectedTab++;
            this.searchMovie(this._searchString, this._selectedTab);
        }
    }
}

// OLD CODE

    /*
    import {Component, OnInit} from '@angular/core';
    import {SearchType} from '../enum/SearchType';

    @Component({
        selector: 'app-home',
        templateUrl: './home.component.html',
        styleUrls: ['./home.component.scss']
    })
    export class HomeComponent implements OnInit {
        private _searchString: string;

        constructor() {
            this._searchString = 'A';

        }

        ngOnInit() {
        }
    */
    /**
     * A chaque changement de recherche dans la bar de recherche, obtient la nouvelle string recherché
     * @param query : la nouvelle string recherché
     */
    /*
    public onSearch(query: string) {
        this.searchString = query;
    }


    get searchString(): string {
        return this._searchString;
    }

    set searchString(value: string) {
        this._searchString = value;
    }

}
*/






